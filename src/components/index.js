import baseMap from './BaseMap/Map';
import mapMarker from './BaseMap/MapMarker';
import selectPlace from './BaseMap/SelectPlace';
import mapPath from './BaseMap/MapPath';
import mapPolyline from './BaseMap/MapPolyline';
import mapCircle from './BaseMap/MapCircle';
import mapAutocomplete from './BaseMap/MapAutocomplete';

import { setConfig } from './config';

export const BaseMap = baseMap;
export const MapMarker = mapMarker;
export const SelectPlace = selectPlace;
export const MapPath = mapPath;
export const MapPolyline = mapPolyline;
export const MapCircle = mapCircle;
export const MapAutocomplete = mapAutocomplete;

export default {
	install(options) {
		setConfig('mapOptions', options);
	},
};

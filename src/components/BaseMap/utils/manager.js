import { getConfig } from '../../config';

var setUp = false;

export const loaded = new Promise((resolve, reject) => {
	if (typeof window === 'undefined') {
		return;
	}
	window['vueGoogleMapsInit'] = resolve;
});

var loadingPromise = null;

export const load = (options, loadCn) => {
	let mapOptions = getConfig('mapOptions');
	if (!loadingPromise)
		loadingPromise = new Promise((resolve, reject) => {
			if (typeof document === 'undefined') {
				return reject();
			}

			if (!setUp) {
				const googleMapScript = document.createElement('SCRIPT');

				if (typeof mapOptions !== 'object') {
					throw new Error('options should  be an object');
				}

				if (Array.prototype.isPrototypeOf(mapOptions.libraries)) {
					mapOptions.libraries = mapOptions.libraries.join(',');
				}
				mapOptions['callback'] = 'vueGoogleMapsInit';

				let baseUrl = 'https://maps.googleapis.com/';

				if (typeof loadCn === 'boolean' && loadCn === true) {
					baseUrl = 'http://maps.google.cn/';
				}

				let url =
					baseUrl +
					'maps/api/js?' +
					Object.keys(mapOptions)
						.map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(mapOptions[key]))
						.join('&');

				googleMapScript.setAttribute('src', url);
				googleMapScript.setAttribute('async', '');
				googleMapScript.setAttribute('defer', '');
				document.head.appendChild(googleMapScript);
				googleMapScript.addEventListener('load', resolve);
			} else {
				reject(new Error('You already started the loading of google maps'));
			}
		});

	return loadingPromise;
};

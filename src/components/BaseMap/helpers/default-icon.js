import { getConfig } from '../../config';

export default () => {
	let configIcon = getConfig('mapOptions').icon;

	if (configIcon == null || configIcon == undefined) {
	    return require('../../../assets/icons/google-map-marker.svg');
    } else {
	    return configIcon;
    }
};

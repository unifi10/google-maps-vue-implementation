import Vue from 'vue';
import App from './App.vue';
import GoogleMaps from './components/index';

Vue.config.productionTip = false;

GoogleMaps.install({
	key: 'AIzaSyBpRIs7w1fBQn_ISEVJZEpuncss0XIYxw0',
	libraries: ['visualization', 'places'],
});

new Vue({
	render: (h) => h(App),
}).$mount('#app');
